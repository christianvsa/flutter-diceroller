import 'dart:math';
import 'package:flutter/material.dart';

final randomizer = Random();

/// A StatefulWidget that creates a dice rolling interface.
///
/// This widget provides a UI for rolling a dice with a dynamic result
/// displayed each time the dice is rolled. The actual dice roll logic
/// and UI updates are handled in its state class [DiceRollerState].
class DiceRoller extends StatefulWidget {
  /// Constructs a DiceRoller widget.
  ///
  /// Takes an optional [Key] that can be used to identify this widget
  /// in the widget tree, which is useful for preserving state or automating
  /// tests that interact with this widget.
  const DiceRoller({Key? key}) : super(key: key);

  @override
  DiceRollerState createState() => DiceRollerState();
}

class DiceRollerState extends State<DiceRoller> with SingleTickerProviderStateMixin {
  int currentDiceRoll = 2;  // Default dice face
  late AnimationController controller;
  late Animation<int> animation;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      duration: const Duration(seconds: 1),
      vsync: this,
    );

    // Setup a default dummy animation to avoid null issues
    animation = IntTween(begin: 2, end: 2).animate(controller)
      ..addListener(() {
        setState(() {
          currentDiceRoll = animation.value;
        });
      });

    controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        controller.reset();
      }
    });
  }

  void rollDice() {
    final newEnd = randomizer.nextInt(6) + 1;

    // Update the animation range
    animation = IntTween(begin: currentDiceRoll, end: newEnd).animate(controller)
      ..addListener(() {
        setState(() {
          currentDiceRoll = animation.value;
        });
      });

    // Start the animation
    controller.forward(from: 0);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        AnimatedSwitcher(
          duration: const Duration(milliseconds: 100),
          transitionBuilder: (Widget child, Animation<double> animation) {
            return ScaleTransition(scale: animation, child: child);
          },
          child: Image.asset(
            'assets/images/dice-$currentDiceRoll.png',
            key: ValueKey<int>(currentDiceRoll),
            width: 200,
          ),
        ),
        const SizedBox(height: 20),
        TextButton(
          onPressed: rollDice,
          style: TextButton.styleFrom(
            foregroundColor: const Color.fromARGB(255, 15, 15, 15),
            backgroundColor: const Color.fromARGB(255, 222, 243, 33),
            textStyle: const TextStyle(fontSize: 28),
          ),
          child: const Text('Roll Dado'),
        ),
      ],
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}
