import 'package:flutter/material.dart';

import 'package:first_app/gradient_container.dart';

void main() {
  runApp(
    const MaterialApp(
      home: Scaffold(
        //here is our widget class
        body: GradientContainer(
          Color.fromARGB(255, 13, 91, 155),
          Color.fromARGB(255, 148, 187, 219),
        ),
      ),
    ),
  );
}
